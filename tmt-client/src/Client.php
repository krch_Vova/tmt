<?php


namespace Vladimir\Tmt;

use Exception;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;
use App\Models\Campaign;
use App\Models\Creative;


class Client
{
    const DEFAULT_PERDAY_SCANS = 12; ///78ac5791bcee911b1fdd4772fe94ede

    const TAG_IMPORT = 'JSON_tag_import'; //https://www.themediatrust.com/api

    const TAG_REMOVE = 'tag_remove';

    const SEARCH_TAG = 'search_tags';

    const ALERT_REPORT = 'alert_report';

    const INCIDENT_REPORT = 'tag_incident_report';

    const TAG_REPORT = 'complete_tag_report';

    const SCAN_PAUSE = 'tag_pause';

    const SCAN_RESUME = 'tag_resume';

    const SCAN_COUNT_REPORT = 'get_tag_level_scan_counts';

    const TMT_API_URL = 'https://s78ac.api.themediatrust.com/v2/';

    const TAG_NAME_PREFIX = 'smartydsp';

    const TAG_SECTION_USER_ID = 1;

    const TAG_SECTION_CREATIVE_ID = 2;

    const TAG_SECTION_CREATIVE_NAME = 3;

    const TYPE_BANNER = 'banner';
    /**
     * @var bool
     */
    private static $useProxy = false;

    /**
     * @var \Illuminate\Config\Repository|mixed
     */
    private $key;

    /**
     * @var
     */
    private $url;

    /**
     * @var
     */
    private $hours;

    /**
     * @var
     */
    private $result;

    /**
     * @var GuzzleClient
     */
    private $client;

    /**
     * @var
     */
    private $response;

    /**
     * @var
     */
    private $error;

    /**
     * @var int
     */
    private $perDayScansCount;

    /**
     * @var
     */
    private $proxy;

    /**
     * TmtApiClient constructor.
     * @param GuzzleClient $guzzleClient
     */
    public function __construct(GuzzleClient $guzzleClient)
    {
        $this->setKey(config('tmt.config.key'));
        $this->setUrl(config('tmt.config.url'));
        $this->setPerDayScansCount(self::DEFAULT_PERDAY_SCANS);

        $this->client = $guzzleClient;
    }

    /**
     * @return array
     */
    private function configureClient(): array
    {
        if(self::$useProxy) {
            return ['proxy' => $this->proxy];
        }

        return [];
    }
    /**
     * @param string $tagName
     * @param int $section
     * @return string|int
     */
    public static function parseTagName(string $tagName, int $section)
    {
        return explode('-', $tagName)[$section] ?? null;
    }

    /**
     * @param Creative $creative
     * @return $this
     */
    public function importTag(Creative $creative)
    {

        $tag = [
            'tagName' => $this->getTagName($creative),
            "meta_batchId" => $creative->userId . '-' . $creative->id,
            'per_day' => $this->perDayScansCount
        ];

        $targetingToImport = [
            'meta_os' => ['include' => 'OSIsInclude', 'field' => 'OS'],
            'meta_browser' => ['include' => 'browserIsInclude', 'field' => 'browser'],
            'country' => ['include' => 'geoCountriesIsInclude', 'field' => 'geoCountries'],

        ];

        /** @var Campaign $campaign */
        $campaign = $creative->campaigns()->first();

        if (!is_null($campaign)) {
            foreach ($targetingToImport as $tagKey => $tarVal) {
                if (!$campaign->{$tarVal['include']} || empty($campaign->{$tarVal['field']})) {
                    continue;
                }

                $tag[$tagKey] = implode(',', $campaign->{$tarVal['field']});
            }
        }

        if ($creative->creativeType !== self::TYPE_BANNER) {

            $this->setResult(null);

            return $this;
        }


        $tag['creativeBase64'] = base64_encode($creative->banner->admBody);

        $tag['runXnow'] = 10;

        return $this->exec(self::TAG_IMPORT, ['enabled_tags' => [$tag]]);
    }

    /**
     * @param Creative $creative
     * @return string
     */
    public function getTagName(Creative $creative)
    {
        return implode('-', [
            self::TAG_NAME_PREFIX,
            $creative->userId,
            $creative->id,
            $creative->name
        ]);
    }

    /**
     * @param $action
     * @param $data
     * @param bool $isId
     * @return $this
     */
    public function exec($action, $data, $isId = false)
    {
        Log::channel('tmt')->debug(
            "Run $action with data: "
            . PHP_EOL . '-------------------------------------' . PHP_EOL
            . var_export($data, true)
            . PHP_EOL . '-------------------------------------' . PHP_EOL
        );

        if (empty($action)) {

            $this->setError('Parameter "action" are empty');

            Log::channel('tmt')->debug($this->getError());

            return $this;
        }


        Log::channel('tmt')->debug("Requesting  TMT: {$this->url}");

        try {
            $this->response = $this->client->post($this->url, [
                'form_params'=> $this->prepareRequest($action, $data, $isId)
            ]);

        } catch (ClientException $clientException) {
            $res = $clientException->getResponse();

            $body = $res->getBody()->getContents();

            Log::channel('tmt')->warning(
                "***FAIL*** Request to TMT  {$this->url}. Status code: {$res->getStatusCode()}. Body: $body"
            );

            $this->setError('Fail to sendout request to TMT API: '.$clientException->getMessage());

            sleep(1);

            return $this;

        } catch (\GuzzleHttp\Exception\RequestException $requestException) {

            $body = $requestException->getRequest()->getBody()->getContents();

            Log::channel('tmt')->warning("***FAIL*** Request to TMT  {$this->url}. Body: $body ");

            $this->setError('Fail to sendout request to TMT API: '.$requestException->getMessage());

            sleep(1);

            return $this;
        }


        if ($this->response->getStatusCode() !== 200) {

            $this->setError('TMT API response with code' . $this->response->getStatusCode());

            Log::channel('tmt')->warning('*** FAIL*** '.$this->getError());

            return $this;
        }

        try {
            $response = $this->response->getBody()->getContents();

            Log::channel('tmt')->debug("Response Body: $response");

            $this->setResult(\GuzzleHttp\json_decode($response, true));

        } catch (Exception $e) {

            $this->setError('TMT API response with msg:' . $response);

            Log::channel('tmt')->warning('*** FAIL*** TMT API response with msg' . $response);
        }

        sleep(1);

        return $this;
    }

    /**
     * @param $action
     * @param $data
     * @param $isId
     * @return array
     */
    private function prepareRequest($action, $data, $isId): array
    {
        $request = [
            'action' => $action,
            'key' => $this->getKey(),
        ];

        switch ($action) {
            case self::TAG_IMPORT:
                $request['data'] = \GuzzleHttp\json_encode($data);

                return $request;

            case self::ALERT_REPORT:
                $this->setHours($data['hours']);
                $request['hours'] = $this->getHours();

                return $request;

            case self::SEARCH_TAG:
                $request['adTagId'] = $data;
                $request['verbose'] = 't';
                if (!$isId) {
                    $request['ad_string'] = $data;
                    unset($request['adTagId']);
                }

                return $request;

            case self::TAG_REPORT:
                $request['adTagId'] = $data;

                return $request;

            case self::SCAN_COUNT_REPORT:
                $request['get_date'] = $data;

                return $request;

            case self::TAG_REMOVE:
            case self::SCAN_PAUSE:
            case self::SCAN_RESUME:
                $request['adTagIds'] = trim(implode(',', $data), ',');

                return $request;

            default:
                $request['hours'] = $this->hours;

                return $request;
        }
    }

    /**
     * @param array $tid
     * @return $this
     */
    public function removeTag(array $tid = [])
    {
        if (count($tid)) {
            return $this->exec(self::TAG_REMOVE, $tid);
        }

        $this->setResult(null);

        return $this;
    }

    /**
     * @param $tid
     * @return $this
     */
    public function completeTagReport($tid)
    {
        return $this->exec(self::TAG_REPORT, $tid);
    }

    /**
     * @param $tid
     * @return $this
     */
    public function pauseTag($tid)
    {
        return $this->exec(self::SCAN_PAUSE, $tid);
    }

    /**
     * @param $tid
     * @return $this
     */
    public function resumeTag($tid)
    {
        return $this->exec(self::SCAN_RESUME, $tid);
    }

    /**
     * @param $date
     * @return $this
     */
    public function scaCountReport($date)
    {
        return $this->exec(self::SCAN_COUNT_REPORT, $date);
    }

    /**
     * @param int $hours
     * @return $this
     */
    public function getAlertReport(int $hours = 0)
    {
        if ($hours > 0) {
            $this->setHours($hours);
        }

        return $this->exec(self::ALERT_REPORT, []);
    }

    /**
     * @param int $hours
     * @return $this
     */
    public function getIncidentReport(int $hours = 0)
    {
        if ($hours > 0) {
            $this->setHours($hours);
        }

        return $this->exec(self::INCIDENT_REPORT, []);
    }

    /**
     * @param $string
     * @param bool $is_id
     * @return $this
     */
    public function searchTag($string, $is_id = false)
    {
        return $this->exec(self::SEARCH_TAG, $string, $is_id);
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->error;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey(string $key): void
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return int
     */
    public function getHours(): int
    {
        return $this->hours;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @return int
     */
    public function getPerDayScansCount(): int
    {
        return $this->perDayScansCount;
    }

    /**
     * @param int $perDayScansCount
     */
    public function setPerDayScansCount(int $perDayScansCount): void
    {
        $this->perDayScansCount = $perDayScansCount;
    }

    /**
     * @return mixed
     */
    public function getProxy()
    {
        return $this->proxy;
    }

    /**
     * @param mixed $proxy
     */
    public function setProxy($proxy): void
    {
        $this->proxy = $proxy;
    }

    /**
     * @return GuzzleClient
     */
    public function getClient(): GuzzleClient
    {
        return $this->client;
    }

    /**
     * @param mixed $error
     */
    public function setError($error): void
    {
        $this->setResult(null);

        $this->error = $error;

    }

    /**
     * @param mixed $result
     */
    public function setResult($result): void
    {
        $this->result = $result;
    }

    /**
     * @param bool $hours
     * @return $this
     */
    public function setHours($hours = false)
    {
        $this->hours = $hours;

        return $this;
    }

    /**
     * @param string $key
     */
    public function setAPIkey(string $key)
    {
        $this->key = $key;
    }

    /**
     * @return array|null
     */
    public function getResult()
    {
        return $this->result;
    }

}
