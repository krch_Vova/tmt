<?php
namespace Vladimir\Tmt;

use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class TmtServiceProvider extends ServiceProvider
{

    /**
     *
     */
    public function boot()
    {
        $this->publishes([__DIR__ . '/../config/' => config_path() . '/']);
    }

    public function register()
    {

    }
}
