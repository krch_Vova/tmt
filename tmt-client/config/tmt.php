<?php

return [

    'models' => [
        'campaign' => null,

        'creative' => null,
    ],

    'config' => [

        'key' => env('TMT_API_KEY', ''),

        'url' => env('TMT_API_URL', 'https://s78ac.api.themediatrust.com/v2/'),

        'alert_email' => ['lusine.tsaturyan@smartyads.com', 'andrey@smartyads.com', 'vitaliy@smartyads.com'],

        'default_perday_scans' => 12, ///78ac5791bcee911b1fdd4772fe94ede

        'tag_import' => 'JSON_tag_import', //https://www.themediatrust.com/api

        'tag_remove' => 'tag_remove',

        'search_tag' => 'search_tags',

        'alert_report' => 'alert_report',

        'incident_report' => 'tag_incident_report',

        'tag_report' => 'complete_tag_report',

        'scan_pause' => 'tag_pause',

        'scan_resume' => 'tag_resume',

        'scan_count_report' => 'get_tag_level_scan_counts',

        'tmt_api_url' => 'https://s78ac.api.themediatrust.com/v2/',

        'tag_name_prefix' => 'smartydsp',

        'tag_section_user_id' => 1,

        'tag_section_creative_id' => 2,

        'tag_section_creative_name' => 3,
    ]
];
